<%-- 
    Document   : comboDetails
    Created on : May 20, 2023, 5:45:51 PM
    Author     : Asus
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ComboDetails</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <link href="../stylesheet/comboDetails.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link
    href="https://fonts.googleapis.com/css2?family=Alegreya&family=MuseoModerno:wght@200;300;400;500;600;800;900&family=Open+Sans:ital,wght@0,300;1,300&display=swap"
    rel="stylesheet">
</head>
<body>
    <jsp:include page="../components/Menu.jsp"></jsp:include>
    <div class="combo1 row"> 
        
        <h2>New Combo</h2>
        
        <<form action="action" method="post">
        
        <div class="combolist">
        <c:forEach items="${requestScope.combodetail}" var="o">
            <div class="card" >
                <a href="comboDetails.html">
                     <c:forEach items="${requestScope.listimg}" var="z">
                        
                        <c:if test="${o.proID == z.proID}">
                            <img src="../${z.proImg}" alt="John" style="width:100%">
                        </c:if>
                      
                    </c:forEach>
                <p class="title">${o.proName}</p>               
                <div class="size ">
                    <span style="font-weight: bold; width: 50px; height: 30px;">Size: </span>
                    <span>M</span>
                    <span>L</span> 
                    <span>XL</span>
                    <span>XXL</span>
                </div>
                <div class="color">
                    <span style="font-weight: bold;">Color: </span>
                    <span style="background-color: red; color: red;">Red</span>
                    <span style="background-color: blue; color: blue;">Red</span>
                    <span style="background-color: black;">Red</span>
                    <span style="background-color: white; color: white;">Red</span>
                </div>
                </a>    
            </div>
        </c:forEach>

        <div class="Price-and-addToCart">
            <p style="direction: rtl; font-size: 30px; color: red;">Price: 100.000VND</p>
            <button style="float: right">Add To Cart</button>
        </div>
    </div>
    </form>
    </div>
    
    <jsp:include page="../components/Footer.jsp"></jsp:include>
    </body>
</html>
    
