<%-- 
    Document   : shopAll
    Created on : May 20, 2023, 3:51:26 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ShopAll</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="../stylesheet/shopAll.css">
        <link
            href="https://fonts.googleapis.com/css2?family=Alegreya&family=MuseoModerno:wght@200;300;400;500;600;800;900&family=Open+Sans:ital,wght@0,300;1,300&display=swap"
            rel="stylesheet">
        <link rel=”stylesheet” href=”https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css”> <script
        src=”https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js”></script> <script
        src=”https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js”></script> <script
        src=”https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js”></script> 
    </head> <!-- Load an icon
        library -->

    <body>
        <div class="wel">
            <a>  <h6 ><strong>Welcome</strong> to our shop</h6></a>

        </div>
        <!-- Top navigation -->
        <div class="topnav">

            <!-- Centered link -->
            <div class="topnav-centered">
                <a href="home.html" class="active"
                   style="color: black; font-size: 40px; font-family: 'MuseoModerno', cursive;"><strong>Front:</strong></a>
            </div>

            <!-- Left-aligned links (default) -->
            <a href="shopAll?caID=0">ShopAll</a>
            <a href="#contact">Contact</a>
            <a href="#about">Profile</a>
            <!-- Right-aligned links -->

            <div class="topnav-right">

                <a style="font-size: 20px;margin-left: 5px;" href="cart.html"> <i class="fa fa-shopping-cart"
                                                                                  style="font-size:25px; height: fit-content;"></i>10</a>

                <a href="">Signout</a>
            </div>




        </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
        <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-3 blog-form">
                        <h2 class="blog-siderbar-title"><b>Categories</b> </h2>
                        <hr />
                        <c:forEach items="${requestScope.list}" var="o">
                            <a href="shopAll?caID=${o.caID}"> <p class="blog-sidebar-list"><b>${o.caName}</b> </p></a> 
                                    </c:forEach>


                        <hr />
                        <!--search -->
                        <h4 class="blog-siderbar-title"><b>Search</b> </h4>
                        <div>
                            <div class="input-group mb-3">
                                <form class="d-flex">
                                    <input type="text" class="form-control" name="search" placeholder="Search">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-search"></i></span>

                                </form>
                            </div>
                        </div>
                        <!--tim theo gia-->
                        <hr />
                        <h4 class="blog-siderbar-title"><b>Price</b> </h4>

                    </div>
                    <!--Ket thuc blog form-->

                    <div class="col-9">
                        <div class="row">
                            <div class="col-9">
                                Showing all ${requestScope.size} result
                            </div>
                            <div class="col-3 ">
                                <form action="../view/shopAll" method="post">
                                    <select class="form-select" name="sort_product">
                                        <option value="new">NEW</option></a>
                                        <option value="low">Low to High</option></a>
                                        <option value="high">High to Low</option>
                                    </select>
                                    <button type="submit" >SORT</button>
                                </form>
                            </div>
                        </div>
                        <!--Sorting-->

                        <div class="row d-flex justify-content-center">


                            <c:forEach items="${requestScope.listpage}" var="i">
                                <div class="col-4 product-field">
                                    <ul class="items">
                                        <li data-category data-price="${i.proPrice}">
                                            <a href="${i.proID}">
                                                <div class="card border border-0">
                                                    <div class="card-body text-center">
                                                        <c:forEach items="${requestScope.list_img}" var="o">
                                                            <c:if test="${i.proID==o.proID}">  
                                                                <img src="../${o.proImg}" class="product-image img-fluid" alt="">
                                                            </c:if>  
                                                        </c:forEach> 
                                                        <h5 class="card-title"><b>${i.proName}</b></h5>

                                                        <p style="color: black; font-size: 18px" class="tags"> <b>${i.proPrice} VND</b></p>

                                                    </div>

                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </c:forEach>

                        </div>
                        <c:set var="page" value="${requestScope.page}"/>
                        <c:set var="caID" value="${requestScope.caID}"/>
                        <c:set var="sortp" value="${requestScope.sort}"/>
                        <div class="pagination" style="margin: auto; justify-content: center; ">



                            <c:if test="${requestScope.page>1}">
                                <li ><a href="shopAll?caID=${caID==null?"0":caID}&page=${page-1}">&laquo;</a></li>
                                </c:if>

                            <c:forEach begin="${1}" end="${requestScope.num}" var="i">
                                <li class="${(i==page)?"active":""}"> <a href= "shopAll?caID=${caID==null?"0":caID}&page=${i}&sort=${sortp==null?"new":sortp}">${i}</a> </li>
                                </c:forEach>
                                <c:if test="${requestScope.page<requestScope.num}">
                                <li><a href="shopAll?caID=${caID==null?"0":caID}&page=${page+1}&sort=${sort}">&raquo;</a></li>
                                </c:if>




                        </div>

                    </div>
                </div>
            </div>

        </section>






        <!--Footer-->
        <footer class="footer" style="display: flex; justify-content: space-between">

            <div class="left" style="width: 30%; text-align: left; margin: 30px; ">
                <p style="font-size: 23px;font-weight: bold">Thank you for your interest in our products</p>
                <p style="font-size: 10px">___________________________________________</p>
                <p>If you are interested in sound and want to experience the product directly, please contact us </p>
                <!-- comment -->
                <p>Address</p>
                <p>11th floor Keangnam Landmark Tower 72 Area E6, Pham Hung, Ha Noi</p>
                <p>Phone: 0999999969</p>



            </div>

            <div class="right" style="width: 30%; text-align: left ;margin: 30px;font-size: 15px">
                <p>We have a mission to change the look of street fashion but not without politeness and seriousness. The
                    company with many years of experience in the fashion industry has developed products that are creative
                    and stylish.</p>
                <p>________________________________________</p>

                <div style="display: flex; margin-top:10px ">

                    <i class="fa fa-instagram fa-fw"></i>
                    <i class="fa fa-twitter fa-fw"></i>
                    <i class="fa fa-google fa-fw"></i>
                    <p></p>




                </div>


        </footer>
    </body>

</html>
