<%-- 
    Document   : productDetail
    Created on : May 20, 2023, 5:37:55 PM
    Author     : ACER
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../stylesheet/productDetail.css">
    <link
        href="https://fonts.googleapis.com/css2?family=Alegreya&family=MuseoModerno:wght@200;300;400;500;600;800;900&family=Open+Sans:ital,wght@0,300;1,300&display=swap"
        rel="stylesheet">
    <link rel=”stylesheet” href=”https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css”> <script
        src=”https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js”> </script> <script
        src=”https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js”> </script> <script
        src=”https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js”> </script> </head> <!-- Load an icon
        library -->

<body>
    <div class="wel">
        <a><h6><strong>Welcome</strong> to our shop</h6></a>

    </div>
    <!-- Top navigation -->
    <div class="topnav">

        <!-- Centered link -->
        <div class="topnav-centered">
            <a href="home.html" class="active"
                style="color: black; font-size: 40px; font-family: 'MuseoModerno', cursive;"><strong>Front:</strong></a>
        </div>

        <!-- Left-aligned links (default) -->
        <a href="shopAll.html">ShopAll</a>
        <a href="#contact">Contact</a>
        <a href="#about">Profile</a>
        <!-- Right-aligned links -->

        <div class="topnav-right">

            <a style="font-size: 20px;margin-left: 5px;" href="cart.html"> <i class="fa fa-shopping-cart"
                    style="font-size:25px; height: fit-content;"></i>10</a>
            
            <a href="">Signout</a>
        </div>
    </div>
    <!--end header-->


    <section class="container sproduct my-2 pt-2">
        <div class="row mt-3 d-flex justify-content-center">
            <div class="col-lg-4 col-md-12 col-12">

                <img class="img-fluid w-100 pb-1" id="MainImg"
                    src="https://pos.nvncdn.net/b3bf61-16762/ps/20221206_c6Kfc1WzHKKqDb7sVCdYsUIs.jpg" alt="">

                <div class="small-img-group ">
                    <div class="small-img-col ">
                        <img src="https://pos.nvncdn.net/b3bf61-16762/ps/20221128_onv9zyYtqlqM54o8JKk7BlBJ.jpg" width="75%" class="small-img" alt="">
                    </div>
                    <div class="small-img-col">
                        <img src="https://pos.nvncdn.net/b3bf61-16762/ps/20221206_c6Kfc1WzHKKqDb7sVCdYsUIs.jpg" width="75%" class="small-img" alt="">
                    </div>
                    <div class="small-img-col">
                        <img src="https://pos.nvncdn.net/b3bf61-16762/ps/20221206_c6Kfc1WzHKKqDb7sVCdYsUIs.jpg" width="75%" class="small-img" alt="">
                    </div>
                    <div class="small-img-col">
                        <img src="https://pos.nvncdn.net/b3bf61-16762/ps/20221206_c6Kfc1WzHKKqDb7sVCdYsUIs.jpg" width="75%" class="small-img" alt="">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-12">

                <h3 class="py-4">Men's Fashion</h3>
                <h4>Ma hang:</h4>
                <h3>$139.00</h3>
                <hr />
                <div class="row">
                    <h5 class="col-4 ">Size</h5>
                    <select class="my-3 col-4 mt-0">
                        <option>Select Size</option>
                        <option>XXL</option>
                        <option>XL</option>
                        <option>L</option>
                        <option>S</option>
                    </select>
                </div>
                
                <div class="row product-quantity flex align-items-center ">
                    <h5 class="col-4">Quantity</h5>
                    <input type="number" id="quantity" class="col-2 px-0 mx-0 text-center" name="quantity" value="1"
                        min="1" max="10">
                    <h6 class="col-6 text-center mt-1"> Con 5 san pham</h6>

                </div>
                &emsp;
                <button class="buy-btn px-3 "><span class="fa fa-cart-plus"></span> Add To Cart</button>

                <span>Detail</span>
            </div>
        </div>

        <section id="featured" class="my-5 pb-5">
            <div class="container text-center mt-5 py-5">
                <h3>San pham lien quan</h3>
                <hr class="mx-auto">
                <p>Nhung san pham moi nhat lien quan den san pham ban lua chon</p>
            </div>
            <div class="row mx-auto container-fluid">
                <div class="product text-center col-lg-3 col-md-4 col-12">
                  <a href="#"><img class="img-fluid mb-3 w-75"
                        src="https://pos.nvncdn.net/b3bf61-16762/ps/20221206_c6Kfc1WzHKKqDb7sVCdYsUIs.jpg" alt=""></a>  
                   <a href="#"> <h5>Candles RD Flowrt Tshirt</h5></a>
                    <h6 style="color: red;"><b>129.00$</b></h6>
                </div>
                <div class="product text-center col-lg-3 col-md-4 col-12">
                    <img class="img-fluid mb-3 w-75"
                        src="https://pos.nvncdn.net/b3bf61-16762/ps/20221206_c6Kfc1WzHKKqDb7sVCdYsUIs.jpg" alt="">
                    <h5>Candles RD Flowrt Tshirt</h5>
                    <h6 style="color: red;"><b>129.00$</b></h6>
                </div>
                <div class="product text-center col-lg-3 col-md-4 col-12">
                    <img class="img-fluid mb-3 w-75"
                        src="https://pos.nvncdn.net/b3bf61-16762/ps/20221206_c6Kfc1WzHKKqDb7sVCdYsUIs.jpg" alt="">
                    <h5>Candles RD Flowrt Tshirt</h5>
                    <h6 style="color: red;"><b>129.00$</b></h6>
                </div>

                <div class="product text-center col-lg-3 col-md-4 col-12">
                    <img class="img-fluid mb-3 w-75"
                        src="https://pos.nvncdn.net/b3bf61-16762/ps/20221206_c6Kfc1WzHKKqDb7sVCdYsUIs.jpg" alt="">
                    <h5>Candles RD Flowrt Tshirt</h5>
                    <h6 style="color: red;"><b>129.00$</b></h6>
                </div>

            </div>
        </section>
    </section>






    <!--Footer-->
    <footer class="footer" style="display: flex; justify-content: space-between">

        <div class="left" style="width: 30%; text-align: left; margin: 30px; ">
            <p style="font-size: 23px;font-weight: bold">Thank you for your interest in our products</p>
            <p style="font-size: 10px">___________________________________________</p>
            <p>If you are interested in sound and want to experience the product directly, please contact us </p>
            <!-- comment -->
            <p>Address</p>
            <p>11th floor Keangnam Landmark Tower 72 Area E6, Pham Hung, Ha Noi</p>
            <p>Phone: 0999999969</p>



        </div>

        <div class="right" style="width: 30%; text-align: left ;margin: 30px;font-size: 15px">
            <p>We have a mission to change the look of street fashion but not without politeness and seriousness. The
                company with many years of experience in the fashion industry has developed products that are creative
                and stylish.</p>
            <p>________________________________________</p>

            <div style="display: flex; margin-top:10px ">

                <i class="fa fa-instagram fa-fw"></i>
                <i class="fa fa-twitter fa-fw"></i>
                <i class="fa fa-google fa-fw"></i>
                <p></p>




            </div>


    </footer>
    <script>
        var MainImg = document.getElementById('MainImg');
        var smallImg = document.getElementsByClassName('small-img');
        
        smallImg[0].onclick = function(){
            MainImg.src = smallImg[0].src;
        }
        smallImg[1].onclick = function(){
            MainImg.src = smallImg[1].src;
        }
        smallImg[2].onclick = function(){
            MainImg.src = smallImg[2].src;
        }
        smallImg[3].onclick = function(){
            MainImg.src = smallImg[3].src;
        }
    </script>
</body>
